<?php
$timestamp = date('Y-m-d H:i:s', time());

//cоздаем подключение к базе данных
$pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud322", 'andreybolonin', 'root');

//проверка на наличие данных заполненных пользователем
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {

    require_once __DIR__ . '/../model/Article.php';

    $article = new Article();
    $article->update($_POST['name'], $_POST['description'], $_POST['created_at'], $_GET['id']);

    //redirect
    header('Location: index.php');

    // view
    require_once __DIR__ . '/../view/update.php';
}
if (isset($_GET['id'])) {

    //подготовка к выполнению sql запроса для выбора записи
    $stmt = $pdo->prepare('SELECT * FROM article WHERE id = :id');

    //заполнение данных в SQL-запрос из $_GET
    $stmt->bindValue(':id', $_GET['id']);

    //выполнение sql запроса
    $stmt->execute();
    $article = $stmt->fetch();
} else {
    header('Location: index.php');
}

// view
require_once __DIR__ . '/../view/update.php';