<?php
//controller

require_once __DIR__ . '/../model/Article.php';

// проверка на наличие данных заполненных пользователем
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {

    $article = new Article();
    $article->insert($_POST['name'], $_POST['description'], $_POST['created_at']);

    // редирект
    header('Location: index.php');
}
$article['name'] = '';
$article['description'] = '';
$article['created_at'] = '';
require_once __DIR__ . '/../view/update.php';