<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

class ControllerTest extends TestCase
{
    Public function testRead()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1.:8001',]);
        $response = $httpClient->request('GET', '/Controller/index.php');
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testCreate()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1.:8001',]);
        $response = $httpClient->request('GET', '/Controller/index.php');
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testUpdate()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1.:8001',]);
        $response = $httpClient->request('GET', '/Controller/index.php');
        $this->assertEquals($response->getStatusCode(), 200);

    }
    public function testDelete()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1.:8001',]);
        $response = $httpClient->request('GET', '/Controller/index.php');
        $this->assertEquals($response->getStatusCode(), 200);
    }
}


