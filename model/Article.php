<?php

// model
class Article
{
    private $dsn = "pgsql:host=127.0.0.1;dbname=crud322";

    public function insert($name, $description, $created_at)
    {
        // создаем подключение к базе данных
        $pdo = new PDO($this->dsn, 'andreybolonin', 'root');

        // подготовка к выполнению sql запроса (добавляем новую запись)
        $stmt = $pdo->prepare('INSERT INTO article (name, description, created_at) VALUES (:name,:description,:created_at)');

        // заполнение данных в SQL-запрос из $_POST
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':description', $description);
        $stmt->bindValue(':created_at', $created_at);

        // выполнение sql запроса
        $stmt->execute();
    }

    public function delete($id)
    {
        // создание подключения к базе данных
        $pdo = new PDO($this->dsn, 'andreybolonin', 'root');

        //подготовка к выполнению sql запроса для удаления записи
        $stmt = $pdo->prepare('DELETE FROM article WHERE id = :id');

        //заполнение данных в SQL-запрос из суперглобальный $_GET
        $stmt->bindValue(':id', $id);

        //выполнение sql запроса
        $stmt->execute();
    }

    public function update($name, $description, $created_at, $id)
    {
      $pdo = new PDO($this->dsn, 'andreybolonin', 'root');
      $stmt = $pdo->prepare('UPDATE article SET name = :name, description = :description, created_at = :created_at WHERE id = :id');
      $stmt->bindValue(':id', $id);
      $stmt->bindValue(':name', $name);
      $stmt->bindValue(':description', $description);
      $stmt->bindValue(':created_at', $created_at);
      $stmt->execute();
      header('Location: index.php');
    }

    public function findAll()
    {
        //создание подключения к базе данных
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud322", 'andreybolonin', 'root');

//подготовка к выполнению sql запроса для обновления записи
        $stmt = $pdo->prepare('SELECT * FROM article ORDER BY created_at');

//выполнение sql запроса
        $stmt->execute();

//получение строк из базы данных
        $articles = $stmt->fetchAll();

        return $articles;

    }

}





